import java.util.Scanner;

public class RabbitHouse{
	public static void main(String[] args){
		String [] arg = new String[2];
		Scanner input = new Scanner(System.in);
		arg = input.nextLine().split(" ");
		if (arg[0].toLowerCase().equals("normal")){
			System.out.println(Integer.toString(IHateRabbit(1, arg[1].length())));
		}
	}

	public static int IHateRabbit(int amountRabbit, int Counter){
		if (Counter <= 1){
			return 1;
		}
		else{
			return amountRabbit*Counter + IHateRabbit(amountRabbit*Counter, Counter-1);
		}
	}
}